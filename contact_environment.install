<?php

/**
 * @file
 * Installs, and uninstalls the contact_environment_variables table for the
 * Contact Environment module.
 */

/**
 * Implements hook_schema().
 */
function contact_environment_schema() {

  $schema['contact_environment_variables'] = array(

    'description' => 'The table for added PHP environment variables.',

    'fields' => array(

      'cid' => array(
        'description' => 'The contact id.',
        'type' => 'numeric',
        'unsigned' => TRUE,
        'not null' => TRUE),

      'variable' => array(
        'description' => 'The PHP environment variable.',
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE),
    ),

    'indexes' => array(
      'cid' => array('cid'),
      'variable' => array('variable'),
    ),

  );

  return $schema;

}

/**
 * Implements hook_enable().
 */
function contact_environment_enable() {

  $admin_role = user_role_load_by_name('administrator');
  user_role_change_permissions($admin_role->rid, array(
    'edit own user contact environment' => TRUE,
    'edit all user contact environment' => TRUE,
    'edit site-wide contact environment' => TRUE,
  ));

  variable_set('contact_environment', NULL);
}

/**
 * Implements hook_install().
 */
function contact_environment_install() {

  //change the module weight to
  //contact_forms weight + 1

  $weight = db_select('system', 's')
              ->fields('s', array('weight'))
              ->condition('name', '[contact_forms]', '=')
              ->execute()
              ->fetchField();

  db_update('system')
    ->fields(array('weight' => $weight +1))
    ->condition('name', '[contact_environment]', '=')
    ->execute();

}


/**
 * Implements hook_uninstall().
 */
function contact_environment_uninstall() {

  variable_del('contact_environment');

  $users = entity_load('user');

  foreach ($users as $user) {

    $account = user_load($user->uid);
    $edit = array();
    $edit['data'] = $account->data;

    if (is_array($edit['data']) and array_key_exists('contact_environment', $edit['data'])) {

      $edit['data']['contact_environment'] = NULL;
      //user_save() only removes the 'contact_environment' elements and does not remove the empty 'contact_environment' from $account->data
      user_save($account, $edit);

    }
  }
}